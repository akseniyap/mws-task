from solution import *
from flask import Flask, request, render_template
from flask_restful import Api

app = Flask(__name__)
api = Api(app)

@app.route('/')
def welcome():
    return render_template("form.html")

@app.route('/result', methods = ['POST'])
def result():
    form = request.form
    record_id = form['record_id']

    vocabulary = titles_bow()
    title = find_title_by(record_id)
    vector = title_vector(vocabulary, title)

    return render_template("result.html", result = vector)

if __name__ == '__main__':
    main()
    app.run()
