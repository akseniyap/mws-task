import csv
import string
import re
from nltk.stem import PorterStemmer
import json

EXTRACTED_COLUMNS = ['Record ID', 'Title']
CONDITION_COLUMN  = 'Languages'
TITLE = 1
FILE_NAME = 'records.csv'

def extract_data(file_name = FILE_NAME):
  with open(file_name) as file:
    reader = csv.reader(file)

    header = next(reader)
    column_indices = [header.index(column) for column in EXTRACTED_COLUMNS]
    condition_column = header.index(CONDITION_COLUMN)

    data = []
    for line in reader:
        if line[condition_column] == "English":
            data.append([line[i] for i in column_indices])

    return data

def remove_punctuation(title):
    return title.translate(None, string.punctuation)

def substitute_numbers(title):
    return re.sub(r'\d+', 'number', title)

def lowercase(title):
    return title.lower()

def remove_non_ascii_characters(title):
    return re.sub(r'[^\x00-\x7f]', '', title)

def sanitize_title(title):
    title = remove_punctuation(title)
    title = substitute_numbers(title)
    title = lowercase(title)
    title = remove_non_ascii_characters(title)

    return title

def sanitize_data(data):
    sanitized = []
    for record_id, title in data:
        sanitized.append([record_id, sanitize_title(title)])

    return sanitized

def stem_title(title):
    porter = PorterStemmer()
    tokens = title.split()
    stemmed = [porter.stem(word) for word in tokens]

    return ' '.join(stemmed)

def stem_data(data):
    stemmed = []
    for record_id, title in data:
        stemmed.append([record_id, stem_title(title)])

    return stemmed

def write_data(data):
    with open('stemmed_data.csv', 'w') as file:
        writer = csv.writer(file)
        writer.writerow(EXTRACTED_COLUMNS)
        writer.writerows(data)

def bag_of_words(input):
    words = []
    for sentence in input:
        words.extend(sentence.split())

    return sorted(set(words))

def title_vector(vocabulary, title):
    words  = title.split()
    vector = []
    for i, word in enumerate(vocabulary):
        vector.append(words.count(word))

    return vector

def titles_bow():
    with open('stemmed_data.csv') as file:
        reader = csv.reader(file)
        next(reader)
        titles = []
        for line in reader:
            titles.append(line[TITLE])

    return bag_of_words(titles)

def serialize_bow(data):
    with open('serialized_bow.json', 'w') as file:
        json.dump(data, file)

def find_title_by(record_id):
    file = csv.reader(open('stemmed_data.csv', 'r'))
    for row in file:
        if record_id in row[0]:
            return row[TITLE]

def main():
    data = extract_data()
    sanitized = sanitize_data(data)
    stemmed = stem_data(sanitized)
    write_data(stemmed)
    vocabulary = titles_bow()
    serialize_bow(vocabulary)
