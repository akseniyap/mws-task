# -*- coding: utf-8 -*-

from solution import *

data = [
    ["(Uk)RN022279075", "Recognition of a probable secundo-primo event in the Early Silurian"],
    ["(Uk)RN022484850", "On Athyris (Brachiopoda) and its type species 'Terebratula' concentrica von Buch"],
    ["(Uk)RN238265573", "Radiolarian assemblages in surface sediments along longitude 175°E in the Pacific Ocean"],
    ["(Uk)RN022484953", "Devonian conodont biochronology in geologic time calibration"],
    ["(Uk)RN023133404", "Evolutionary patterns of Late Permian Albaillella (Radiolaria) as seen in bedded chert sections in the Mino Belt, Japan"],
    ["(Uk)RN002244068", "Impacts of storms on Recent planktic foraminiferal test production and CaCO₃ flux in the North Atlantic at 47°N, 20°W (JGOFS)"]
]

bow_data = [
    ["1", "I like Python."],
    ["2", "John likes Python and also likes REST."]
]

def test_extract_data():
    assert extract_data('sample_records.csv') == data

def test_remove_punctuation(title):
    assert remove_punctuation(title) == "On Athyris Brachiopoda and its type species Terebratula concentrica von Buch"

def test_substitute_numbers(title):
    assert substitute_numbers(title) == "Radiolarian assemblages in surface sediments along longitude number°E in the Pacific Ocean"

def test_lowercase(title):
    assert lowercase(title) == "evolutionary patterns of late permian albaillella (radiolaria) as seen in bedded chert sections in the mino belt, japan"

def test_remove_non_ascii_characters(title):
    assert remove_non_ascii_characters(title) == "Impacts of storms on Recent planktic foraminiferal test production and CaCO flux in the North Atlantic at 47N, 20W (JGOFS)"

def test_sanitize_title(title):
    assert sanitize_title(title) == "impacts of storms on recent planktic foraminiferal test production and caco flux in the north atlantic at numbern numberw jgofs"

def test_sanitize_data(data):
    assert sanitize_data(data) == [
        ["(Uk)RN022279075", "recognition of a probable secundoprimo event in the early silurian"],
        ["(Uk)RN022484850", "on athyris brachiopoda and its type species terebratula concentrica von buch"],
        ["(Uk)RN238265573", "radiolarian assemblages in surface sediments along longitude numbere in the pacific ocean"],
        ["(Uk)RN022484953", "devonian conodont biochronology in geologic time calibration"],
        ["(Uk)RN023133404", "evolutionary patterns of late permian albaillella radiolaria as seen in bedded chert sections in the mino belt japan"],
        ["(Uk)RN002244068", "impacts of storms on recent planktic foraminiferal test production and caco flux in the north atlantic at numbern numberw jgofs"]
]

def test_stem_title(title):
    assert stem_title(title) == "evolutionari pattern of late permian albaillella radiolaria as seen in bed chert section in the mino belt japan"

def test_stem_data(data):
    assert stem_data(data) == [
        ["(Uk)RN022279075", "recognit of a probabl secundoprimo event in the earli silurian"],
        ["(Uk)RN022484850", "on athyri brachiopoda and it type speci terebratula concentrica von buch"],
        ["(Uk)RN238265573", "radiolarian assemblag in surfac sediment along longitud number in the pacif ocean"],
        ["(Uk)RN022484953", "devonian conodont biochronolog in geolog time calibr"],
        ["(Uk)RN023133404", "evolutionari pattern of late permian albaillella radiolaria as seen in bed chert section in the mino belt japan"],
        ["(Uk)RN002244068", "impact of storm on recent planktic foraminifer test product and caco flux in the north atlant at numbern numberw jgof"]
]

def test_bag_of_words(input):
    assert bag_of_words(input) == ["also", "and", "i", "john", "like", "python", "rest"]

def test_title_vector(vocabulary, title):
    assert sorted(title_vector(vocabulary, title)) == sorted([0, 2, 1, 1, 1, 1, 1])

if __name__ == "__main__":
    test_extract_data()

    test_remove_punctuation("On Athyris (Brachiopoda) and its type species 'Terebratula' concentrica von Buch")
    test_substitute_numbers("Radiolarian assemblages in surface sediments along longitude 175°E in the Pacific Ocean")
    test_lowercase("Evolutionary patterns of Late Permian Albaillella (Radiolaria) as seen in bedded chert sections in the Mino Belt, Japan")
    test_remove_non_ascii_characters("Impacts of storms on Recent planktic foraminiferal test production and CaCO₃ flux in the North Atlantic at 47°N, 20°W (JGOFS)")
    test_sanitize_title("Impacts of storms on Recent planktic foraminiferal test production and CaCO₃ flux in the North Atlantic at 47°N, 20°W (JGOFS)")
    test_sanitize_data(data)

    test_stem_title("evolutionary patterns of late permian albaillella radiolaria as seen in bedded chert sections in the mino belt japan")
    test_stem_data(sanitize_data(data))

    sanitized = sanitize_data(bow_data)
    stemmed   = stem_data(sanitized)
    sentences = [element[1] for element in stemmed]
    test_bag_of_words(sentences)

    vocabulary = bag_of_words(sentences)
    test_title_vector(vocabulary, stemmed[1][1])

    print("Everything passed! :)")
